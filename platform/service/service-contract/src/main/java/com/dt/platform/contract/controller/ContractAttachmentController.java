package com.dt.platform.contract.controller;

import java.util.List;
import java.util.ArrayList;
import com.github.foxnic.commons.collection.CollectorUtil;
import com.github.foxnic.dao.entity.ReferCause;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import com.github.foxnic.api.swagger.InDoc;
import org.github.foxnic.web.framework.web.SuperController;
import org.github.foxnic.web.framework.sentinel.SentinelExceptionUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.github.foxnic.api.swagger.ApiParamSupport;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.dt.platform.proxy.contract.ContractAttachmentServiceProxy;
import com.dt.platform.domain.contract.meta.ContractAttachmentVOMeta;
import com.dt.platform.domain.contract.ContractAttachment;
import com.dt.platform.domain.contract.ContractAttachmentVO;
import com.github.foxnic.api.transter.Result;
import com.github.foxnic.dao.data.SaveMode;
import com.github.foxnic.dao.excel.ExcelWriter;
import com.github.foxnic.springboot.web.DownloadUtil;
import com.github.foxnic.dao.data.PagedList;
import java.util.Date;
import java.sql.Timestamp;
import com.github.foxnic.api.error.ErrorDesc;
import com.github.foxnic.commons.io.StreamUtil;
import java.util.Map;
import com.github.foxnic.dao.excel.ValidateResult;
import java.io.InputStream;
import com.dt.platform.domain.contract.meta.ContractAttachmentMeta;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiImplicitParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.dt.platform.contract.service.IContractAttachmentService;

/**
 * <p>
 * 合同附件接口控制器
 * </p>
 * @author 李方捷 , leefangjie@qq.com
 * @since 2022-10-21 15:39:34
 */
@InDoc
@Api(tags = "合同管理/合同附件")
@RestController("ContContractAttachmentController")
public class ContractAttachmentController extends SuperController {

    @Autowired
    private IContractAttachmentService contractAttachmentService;

    /**
     * 添加合同附件
     */
    @ApiOperation(value = "添加合同附件")
    @ApiImplicitParams({ 
		@ApiImplicitParam(name = ContractAttachmentVOMeta.ID, value = "主键", required = true, dataTypeClass = String.class, example = "565281472081035264"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.OWNER_ID, value = "所有者ID", required = false, dataTypeClass = String.class, example = "565281320981233664"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.OWNER_TYPE, value = "所有者类型", required = false, dataTypeClass = String.class, example = "contract"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.TYPE, value = "附件类型", required = false, dataTypeClass = String.class, example = "contract_text"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.FILE_ID, value = "文件ID", required = false, dataTypeClass = String.class, example = "565281467555381248"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.NAME, value = "附件名称", required = false, dataTypeClass = String.class, example = "1212"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.NOTES, value = "备注", required = false, dataTypeClass = String.class)
	})
    @ApiParamSupport(ignoreDBTreatyProperties = true, ignoreDefaultVoProperties = true, ignorePrimaryKey = true)
    @ApiOperationSupport(order = 1, author = "李方捷 , leefangjie@qq.com")
    @SentinelResource(value = ContractAttachmentServiceProxy.INSERT, blockHandlerClass = { SentinelExceptionUtil.class }, blockHandler = SentinelExceptionUtil.HANDLER)
    @PostMapping(ContractAttachmentServiceProxy.INSERT)
    public Result insert(ContractAttachmentVO contractAttachmentVO) {
        Result result = contractAttachmentService.insert(contractAttachmentVO, false);
        return result;
    }

    /**
     * 删除合同附件
     */
    @ApiOperation(value = "删除合同附件")
    @ApiImplicitParams({ 
		@ApiImplicitParam(name = ContractAttachmentVOMeta.ID, value = "主键", required = true, dataTypeClass = String.class, example = "565281472081035264")
	})
    @ApiOperationSupport(order = 2, author = "李方捷 , leefangjie@qq.com")
    @SentinelResource(value = ContractAttachmentServiceProxy.DELETE, blockHandlerClass = { SentinelExceptionUtil.class }, blockHandler = SentinelExceptionUtil.HANDLER)
    @PostMapping(ContractAttachmentServiceProxy.DELETE)
    public Result deleteById(String id) {
        this.validator().asserts(id).require("缺少id值");
        if (this.validator().failure()) {
            return this.validator().getFirstResult();
        }
        // 引用校验
        ReferCause cause =  contractAttachmentService.hasRefers(id);
        // 判断是否可以删除
        this.validator().asserts(cause.hasRefer()).requireEqual("不允许删除当前记录："+cause.message(),false);
        if (this.validator().failure()) {
            return this.validator().getFirstResult();
        }
        Result result = contractAttachmentService.deleteByIdLogical(id);
        return result;
    }

    /**
     * 批量删除合同附件 <br>
     * 联合主键时，请自行调整实现
     */
    @ApiOperation(value = "批量删除合同附件")
    @ApiImplicitParams({ 
		@ApiImplicitParam(name = ContractAttachmentVOMeta.IDS, value = "主键清单", required = true, dataTypeClass = List.class, example = "[1,3,4]")
	})
    @ApiOperationSupport(order = 3, author = "李方捷 , leefangjie@qq.com")
    @SentinelResource(value = ContractAttachmentServiceProxy.DELETE_BY_IDS, blockHandlerClass = { SentinelExceptionUtil.class }, blockHandler = SentinelExceptionUtil.HANDLER)
    @PostMapping(ContractAttachmentServiceProxy.DELETE_BY_IDS)
    public Result deleteByIds(List<String> ids) {
        // 参数校验
        this.validator().asserts(ids).require("缺少ids参数");
        if (this.validator().failure()) {
            return this.validator().getFirstResult();
        }
        // 查询引用
        Map<String, ReferCause> causeMap = contractAttachmentService.hasRefers(ids);
        // 收集可以删除的ID值
        List<String> canDeleteIds = new ArrayList<>();
        for (Map.Entry<String, ReferCause> e : causeMap.entrySet()) {
            if (!e.getValue().hasRefer()) {
                canDeleteIds.add(e.getKey());
            }
        }
        // 执行删除
        if (canDeleteIds.isEmpty()) {
            // 如果没有一行可以被删除
            return ErrorDesc.failure().message("无法删除您选中的数据行：").data(0)
				.addErrors(CollectorUtil.collectArray(CollectorUtil.filter(causeMap.values(),(e)->{return e.hasRefer();}),ReferCause::message,String.class))
				.messageLevel4Confirm();
        } else if (canDeleteIds.size() == ids.size()) {
            // 如果全部可以删除
            Result result = contractAttachmentService.deleteByIdsLogical(canDeleteIds);
            return result;
        } else if (canDeleteIds.size() > 0 && canDeleteIds.size() < ids.size()) {
            // 如果部分行可以删除
            Result result = contractAttachmentService.deleteByIdsLogical(canDeleteIds);
            if (result.failure()) {
                return result;
            } else {
                return ErrorDesc.success().message("已删除 " + canDeleteIds.size() + " 行，但另有 " + (ids.size() - canDeleteIds.size()) + " 行数据无法删除").data(canDeleteIds.size())
					.addErrors(CollectorUtil.collectArray(CollectorUtil.filter(causeMap.values(),(e)->{return e.hasRefer();}),ReferCause::message,String.class))
					.messageLevel4Confirm();
            }
        } else {
            // 理论上，这个分支不存在
            return ErrorDesc.success().message("数据删除未处理");
        }
    }

    /**
     * 更新合同附件
     */
    @ApiOperation(value = "更新合同附件")
    @ApiImplicitParams({ 
		@ApiImplicitParam(name = ContractAttachmentVOMeta.ID, value = "主键", required = true, dataTypeClass = String.class, example = "565281472081035264"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.OWNER_ID, value = "所有者ID", required = false, dataTypeClass = String.class, example = "565281320981233664"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.OWNER_TYPE, value = "所有者类型", required = false, dataTypeClass = String.class, example = "contract"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.TYPE, value = "附件类型", required = false, dataTypeClass = String.class, example = "contract_text"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.FILE_ID, value = "文件ID", required = false, dataTypeClass = String.class, example = "565281467555381248"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.NAME, value = "附件名称", required = false, dataTypeClass = String.class, example = "1212"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.NOTES, value = "备注", required = false, dataTypeClass = String.class)
	})
    @ApiParamSupport(ignoreDBTreatyProperties = true, ignoreDefaultVoProperties = true)
    @ApiOperationSupport(order = 4, author = "李方捷 , leefangjie@qq.com", ignoreParameters = { ContractAttachmentVOMeta.PAGE_INDEX, ContractAttachmentVOMeta.PAGE_SIZE, ContractAttachmentVOMeta.SEARCH_FIELD, ContractAttachmentVOMeta.FUZZY_FIELD, ContractAttachmentVOMeta.SEARCH_VALUE, ContractAttachmentVOMeta.DIRTY_FIELDS, ContractAttachmentVOMeta.SORT_FIELD, ContractAttachmentVOMeta.SORT_TYPE, ContractAttachmentVOMeta.IDS })
    @SentinelResource(value = ContractAttachmentServiceProxy.UPDATE, blockHandlerClass = { SentinelExceptionUtil.class }, blockHandler = SentinelExceptionUtil.HANDLER)
    @PostMapping(ContractAttachmentServiceProxy.UPDATE)
    public Result update(ContractAttachmentVO contractAttachmentVO) {
        Result result = contractAttachmentService.update(contractAttachmentVO, SaveMode.DIRTY_OR_NOT_NULL_FIELDS, false);
        return result;
    }

    /**
     * 保存合同附件
     */
    @ApiOperation(value = "保存合同附件")
    @ApiImplicitParams({ 
		@ApiImplicitParam(name = ContractAttachmentVOMeta.ID, value = "主键", required = true, dataTypeClass = String.class, example = "565281472081035264"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.OWNER_ID, value = "所有者ID", required = false, dataTypeClass = String.class, example = "565281320981233664"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.OWNER_TYPE, value = "所有者类型", required = false, dataTypeClass = String.class, example = "contract"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.TYPE, value = "附件类型", required = false, dataTypeClass = String.class, example = "contract_text"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.FILE_ID, value = "文件ID", required = false, dataTypeClass = String.class, example = "565281467555381248"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.NAME, value = "附件名称", required = false, dataTypeClass = String.class, example = "1212"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.NOTES, value = "备注", required = false, dataTypeClass = String.class)
	})
    @ApiParamSupport(ignoreDBTreatyProperties = true, ignoreDefaultVoProperties = true)
    @ApiOperationSupport(order = 5, ignoreParameters = { ContractAttachmentVOMeta.PAGE_INDEX, ContractAttachmentVOMeta.PAGE_SIZE, ContractAttachmentVOMeta.SEARCH_FIELD, ContractAttachmentVOMeta.FUZZY_FIELD, ContractAttachmentVOMeta.SEARCH_VALUE, ContractAttachmentVOMeta.DIRTY_FIELDS, ContractAttachmentVOMeta.SORT_FIELD, ContractAttachmentVOMeta.SORT_TYPE, ContractAttachmentVOMeta.IDS })
    @SentinelResource(value = ContractAttachmentServiceProxy.SAVE, blockHandlerClass = { SentinelExceptionUtil.class }, blockHandler = SentinelExceptionUtil.HANDLER)
    @PostMapping(ContractAttachmentServiceProxy.SAVE)
    public Result save(ContractAttachmentVO contractAttachmentVO) {
        Result result = contractAttachmentService.save(contractAttachmentVO, SaveMode.DIRTY_OR_NOT_NULL_FIELDS, false);
        return result;
    }

    /**
     * 获取合同附件
     */
    @ApiOperation(value = "获取合同附件")
    @ApiImplicitParams({ 
		@ApiImplicitParam(name = ContractAttachmentVOMeta.ID, value = "主键", required = true, dataTypeClass = String.class, example = "1")
	})
    @ApiOperationSupport(order = 6, author = "李方捷 , leefangjie@qq.com")
    @SentinelResource(value = ContractAttachmentServiceProxy.GET_BY_ID, blockHandlerClass = { SentinelExceptionUtil.class }, blockHandler = SentinelExceptionUtil.HANDLER)
    @PostMapping(ContractAttachmentServiceProxy.GET_BY_ID)
    public Result<ContractAttachment> getById(String id) {
        Result<ContractAttachment> result = new Result<>();
        ContractAttachment contractAttachment = contractAttachmentService.getById(id);
        result.success(true).data(contractAttachment);
        return result;
    }

    /**
     * 批量获取合同附件 <br>
     * 联合主键时，请自行调整实现
     */
    @ApiOperation(value = "批量获取合同附件")
    @ApiImplicitParams({ 
		@ApiImplicitParam(name = ContractAttachmentVOMeta.IDS, value = "主键清单", required = true, dataTypeClass = List.class, example = "[1,3,4]")
	})
    @ApiOperationSupport(order = 3, author = "李方捷 , leefangjie@qq.com")
    @SentinelResource(value = ContractAttachmentServiceProxy.GET_BY_IDS, blockHandlerClass = { SentinelExceptionUtil.class }, blockHandler = SentinelExceptionUtil.HANDLER)
    @PostMapping(ContractAttachmentServiceProxy.GET_BY_IDS)
    public Result<List<ContractAttachment>> getByIds(List<String> ids) {
        Result<List<ContractAttachment>> result = new Result<>();
        List<ContractAttachment> list = contractAttachmentService.queryListByIds(ids);
        result.success(true).data(list);
        return result;
    }

    /**
     * 查询合同附件
     */
    @ApiOperation(value = "查询合同附件")
    @ApiImplicitParams({ 
		@ApiImplicitParam(name = ContractAttachmentVOMeta.ID, value = "主键", required = true, dataTypeClass = String.class, example = "565281472081035264"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.OWNER_ID, value = "所有者ID", required = false, dataTypeClass = String.class, example = "565281320981233664"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.OWNER_TYPE, value = "所有者类型", required = false, dataTypeClass = String.class, example = "contract"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.TYPE, value = "附件类型", required = false, dataTypeClass = String.class, example = "contract_text"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.FILE_ID, value = "文件ID", required = false, dataTypeClass = String.class, example = "565281467555381248"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.NAME, value = "附件名称", required = false, dataTypeClass = String.class, example = "1212"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.NOTES, value = "备注", required = false, dataTypeClass = String.class)
	})
    @ApiOperationSupport(order = 5, author = "李方捷 , leefangjie@qq.com", ignoreParameters = { ContractAttachmentVOMeta.PAGE_INDEX, ContractAttachmentVOMeta.PAGE_SIZE })
    @SentinelResource(value = ContractAttachmentServiceProxy.QUERY_LIST, blockHandlerClass = { SentinelExceptionUtil.class }, blockHandler = SentinelExceptionUtil.HANDLER)
    @PostMapping(ContractAttachmentServiceProxy.QUERY_LIST)
    public Result<List<ContractAttachment>> queryList(ContractAttachmentVO sample) {
        Result<List<ContractAttachment>> result = new Result<>();
        List<ContractAttachment> list = contractAttachmentService.queryList(sample);
        result.success(true).data(list);
        return result;
    }

    /**
     * 分页查询合同附件
     */
    @ApiOperation(value = "分页查询合同附件")
    @ApiImplicitParams({ 
		@ApiImplicitParam(name = ContractAttachmentVOMeta.ID, value = "主键", required = true, dataTypeClass = String.class, example = "565281472081035264"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.OWNER_ID, value = "所有者ID", required = false, dataTypeClass = String.class, example = "565281320981233664"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.OWNER_TYPE, value = "所有者类型", required = false, dataTypeClass = String.class, example = "contract"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.TYPE, value = "附件类型", required = false, dataTypeClass = String.class, example = "contract_text"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.FILE_ID, value = "文件ID", required = false, dataTypeClass = String.class, example = "565281467555381248"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.NAME, value = "附件名称", required = false, dataTypeClass = String.class, example = "1212"),
		@ApiImplicitParam(name = ContractAttachmentVOMeta.NOTES, value = "备注", required = false, dataTypeClass = String.class)
	})
    @ApiOperationSupport(order = 8, author = "李方捷 , leefangjie@qq.com")
    @SentinelResource(value = ContractAttachmentServiceProxy.QUERY_PAGED_LIST, blockHandlerClass = { SentinelExceptionUtil.class }, blockHandler = SentinelExceptionUtil.HANDLER)
    @PostMapping(ContractAttachmentServiceProxy.QUERY_PAGED_LIST)
    public Result<PagedList<ContractAttachment>> queryPagedList(ContractAttachmentVO sample) {
        Result<PagedList<ContractAttachment>> result = new Result<>();
        PagedList<ContractAttachment> list = contractAttachmentService.queryPagedList(sample, sample.getPageSize(), sample.getPageIndex());
        result.success(true).data(list);
        return result;
    }
}
